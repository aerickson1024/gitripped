module.exports.data = [
  {
    "firstname": "Carolina",
    "lastname": "in",
    "age": 70,
    "sex": "female",
    "occupation": "quis sint"
  },
  {
    "firstname": "Herring",
    "lastname": "irure",
    "age": 10,
    "sex": "male",
    "occupation": "consequat velit"
  },
  {
    "firstname": "Reynolds",
    "lastname": "incididunt",
    "age": 31,
    "sex": "male",
    "occupation": "irure nisi"
  },
  {
    "firstname": "Martinez",
    "lastname": "mollit",
    "age": 21,
    "sex": "male",
    "occupation": "velit laboris"
  },
  {
    "firstname": "Skinner",
    "lastname": "dolor",
    "age": 60,
    "sex": "male",
    "occupation": "incididunt ad"
  },
  {
    "firstname": "Eliza",
    "lastname": "laboris",
    "age": 16,
    "sex": "female",
    "occupation": "in ipsum"
  },
  {
    "firstname": "Ryan",
    "lastname": "reprehenderit",
    "age": 29,
    "sex": "male",
    "occupation": "nulla aliqua"
  },
  {
    "firstname": "Winnie",
    "lastname": "commodo",
    "age": 73,
    "sex": "female",
    "occupation": "aliquip ea"
  },
  {
    "firstname": "Kim",
    "lastname": "id",
    "age": 15,
    "sex": "female",
    "occupation": "proident aliquip"
  },
  {
    "firstname": "Reid",
    "lastname": "deserunt",
    "age": 60,
    "sex": "male",
    "occupation": "excepteur anim"
  },
  {
    "firstname": "Colon",
    "lastname": "exercitation",
    "age": 78,
    "sex": "male",
    "occupation": "commodo exercitation"
  },
  {
    "firstname": "Shields",
    "lastname": "anim",
    "age": 62,
    "sex": "male",
    "occupation": "est veniam"
  },
  {
    "firstname": "Earlene",
    "lastname": "eiusmod",
    "age": 66,
    "sex": "female",
    "occupation": "mollit sint"
  },
  {
    "firstname": "Shari",
    "lastname": "in",
    "age": 42,
    "sex": "female",
    "occupation": "aute ad"
  },
  {
    "firstname": "Tami",
    "lastname": "aliquip",
    "age": 59,
    "sex": "female",
    "occupation": "enim dolore"
  },
  {
    "firstname": "Little",
    "lastname": "pariatur",
    "age": 5,
    "sex": "male",
    "occupation": "sint elit"
  },
  {
    "firstname": "Chris",
    "lastname": "tempor",
    "age": 57,
    "sex": "female",
    "occupation": "magna veniam"
  },
  {
    "firstname": "Gracie",
    "lastname": "excepteur",
    "age": 3,
    "sex": "female",
    "occupation": "qui occaecat"
  },
  {
    "firstname": "Garrett",
    "lastname": "reprehenderit",
    "age": 77,
    "sex": "male",
    "occupation": "cupidatat consequat"
  },
  {
    "firstname": "Harrell",
    "lastname": "ad",
    "age": 76,
    "sex": "male",
    "occupation": "velit velit"
  }
];
