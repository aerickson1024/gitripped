(function(){
    angular
        .module('app.main')
        .controller('main', Main);

    Main.$inject = ['service'];

    function Main(service){
        var vm = this;
        service.getData(function(data){
            vm.data = data.data;
        });
    }
}());
