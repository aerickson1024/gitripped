(function(){
    angular
        .module('app.main')
        .service('service', Service);

    Service.$inject = ['$http'];

    function Service($http){
        this.getData = function(callback){
            var data = [
                {
                    firstname: 'andy',
                    lastname: 'erickson',
                    age: 34,
                    sex: 'male',
                    occupation: 'software engineer'
                }
            ];
            $http.post('/api/data', data).then(function(response){
                callback(response);
            });
        }
    }
}());
