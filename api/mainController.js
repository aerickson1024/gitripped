var bodyParser = require('body-Parser');
var model = require('../models/data');

module.exports = function(app){
    app.use(bodyParser.json());
    app.use(bodyParser.urlencoded({ extended: true }));

    app.post('/api/data', function(req, res){
        // req.body.forEach(function(item){
        //     item.firstname = 'jodie';
        //     item.lastname = 'houle';
        //     item.age = 32;
        //     item.sex = 'female';
        //     item.occupation = 'seller support';
        // });

        res.send(model.data);
    });
}
