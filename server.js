var express = require('express'),
    mainController = require('./api/mainController'),
    app = express(),
    port = process.env.PORT || 3000;

app.use(express.static(__dirname + '/public'));

mainController(app);

app.listen(port, function(){ console.log('Server listening on port %s', port); });
